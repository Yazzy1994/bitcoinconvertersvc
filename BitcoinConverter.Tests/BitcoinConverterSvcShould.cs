using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using Xunit;

namespace
CloudAcademy.
Bitcoin.
Tests //Namespace needs to be same as the class namespace we are testning
{
    public class BitcoinConverterSvcShould
    {
        private const string
            MOCK_RESPONSE_JSON
            =
            @"{""time"": {""updated"": ""Oct 15, 2020 22:55:00 UTC"",""updatedISO"": ""2020-10-15T22:55:00+00:00"",""updateduk"": ""Oct 15, 2020 at 23:55 BST""},""chartName"": ""Bitcoin"",""bpi"": {""USD"": {""code"": ""USD"",""symbol"": ""&#36;"",""rate"": ""11,486.5341"",""description"": ""United States Dollar"",""rate_float"": 11486.5341},""GBP"": {""code"": ""GBP"",""symbol"": ""&pound;"",""rate"": ""8,900.8693"",""description"": ""British Pound Sterling"",""rate_float"": 8900.8693},""EUR"": {""code"": ""EUR"",""symbol"": ""&euro;"",""rate"": ""9,809.3278"",""description"": ""Euro"",""rate_float"": 9809.3278}}}"; //private constant that contains what comes back from CoinDesk API

        private ConverterSvc mockConverter;

        public BitcoinConverterSvcShould() //Default constructor without paramenters
        {
            // so when our unit tests start up, the default constructor gets called and of which a mockConverter is created

            //arrange
            mockConverter = GetMockBitcoinConverterService();
        }

        private ConverterSvc
        GetMockBitcoinConverterService() // Here is where our mocking implementation takes places
        {
            // This method returns a ConverterSvc. The background to this is that we build a mock object that gets injected into any tine an HTTP calls is made to our API point
            var handlerMock = new Mock<HttpMessageHandler>();
            var response = //Here we are setting the response to contain MOCK_RESPONSE_JSON payload
                new HttpResponseMessage {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(MOCK_RESPONSE_JSON)
                };

            _ = handlerMock // The response that is above is called upon any time the mock is called upon
                .Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(response);

            var httpClient = new HttpClient(handlerMock.Object); // a new HTTP client into which our mocking implementation is pass in. There for anytime that client gets called it's going to return that static response

            var converter = new ConverterSvc(httpClient); // a new Converter service, whereby our HTTP client which has our HTTP response mock data in it, is passed in

            return converter; // That converter is return
        }

        [Fact]
        public async void GetExchangeRate_USD_ReturnsUSDExchangeRate() // GetExchangeRate = the method under test, USD = state used within the test (So our input currency),  ReturnsUSDExchangeRate = expected response
        {
            //arrange
            //act
            var exchangeRate = await mockConverter.GetExchangeRate(ConverterSvc.Currency.USD);

            //assert
            var expected = 11486.5341;
            Assert.Equal (expected, exchangeRate);
        }

        [Fact]
        public async void GetExchangeRate_GBP_ReturnsUSDExchangeRate() // GetExchangeRate = the method under test, USD = state used within the test (So our input currency),  ReturnsUSDExchangeRate = expected response
        {
            //arrange
            //act
            var exchangeRate = await mockConverter.GetExchangeRate(ConverterSvc.Currency.GBP);

            //assert
            var expected = 8900.8693;
            Assert.Equal (expected, exchangeRate);
        }

        [Fact]
        public async void GetExchangeRate_EUR_ReturnsUSDExchangeRate() // GetExchangeRate = the method under test, USD = state used within the test (So our input currency),  ReturnsUSDExchangeRate = expected response
        {
            //arrange
            //act
            var exchangeRate = await mockConverter.GetExchangeRate(ConverterSvc.Currency.EUR);

            //assert
            var expected = 9809.3278;
            Assert.Equal (expected, exchangeRate);
        }

        [Theory] //Theory works with inline data attribute
        [InlineData(ConverterSvc.Currency.USD, 1, 11486.5341)] //Currency, amount of bitcoins, expected output. We can have multiple inlinedatas
        [InlineData(ConverterSvc.Currency.USD, 2, 22973.0682)]
        [InlineData(ConverterSvc.Currency.GBP, 1, 8900.8693)]
        [InlineData(ConverterSvc.Currency.GBP, 2, 17801.7386)]
        [InlineData(ConverterSvc.Currency.EUR, 1, 9809.3278)]
        [InlineData(ConverterSvc.Currency.EUR, 2, 19618.6556)]
        public async void ConvertBitcoins_BitcoinsToCurrency_ReturnsCurrency(
            ConverterSvc.Currency currency,
            int coins,
            double convertedDollars
        )
        {
            //act
            var exchangeRate =
                await mockConverter.ConvertBitcoins(currency, coins);

            //assert
            var expected = convertedDollars;
            Assert.Equal (expected, exchangeRate);
        }

        [Fact]
        public
        async void ConvertBitcoins_BitcoinsAPIServiceUnavailable_ReturnsNegativeOne()
        {

             // This method returns a ConverterSvc. The background to this is that we build a mock object that gets injected into any tine an HTTP calls is made to our API point
            var handlerMock = new Mock<HttpMessageHandler>();
            var response = //Here we are setting the response to contain MOCK_RESPONSE_JSON payload
                new HttpResponseMessage { // The key line items are these ones 
                    StatusCode = HttpStatusCode.ServiceUnavailable,
                    Content = new StringContent("problems ...")
                };

            handlerMock // The response that is above is called upon any time the mock is called upon
                .Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync",
                ItExpr.IsAny<HttpRequestMessage>(),
                ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(response);

            var httpClient = new HttpClient(handlerMock.Object); // a new HTTP client into which our mocking implementation is pass in. There for anytime that client gets called it's going to return that static response

            var converter = new ConverterSvc(httpClient); // a new Converter service, whereby our HTTP client which has our HTTP response mock data in it, is passed in

            var amount = await converter.ConvertBitcoins(ConverterSvc.Currency.USD, 5);

            var expected = -1; 
            Assert.Equal(expected, amount);

        }


        [Fact]
        public async void ConvertBitcoins_BitcoinsLessThanZere_ThrowsArgumentException() { 

            // act
            Task result() => mockConverter.ConvertBitcoins(ConverterSvc.Currency.USD, -6); 


            //Assert
            await Assert.ThrowsAsync<ArgumentException>(result); 


        }







    }



}
